# Linkliste für die Migration auf Java 8 #
------------------------------------------

## Artifaktmanager ##

Artifactory
: [http://www.jfrog.com/home/v_artifactory_opensource_overview](http://www.jfrog.com/home/v_artifactory_opensource_overview)

Nexus
: [http://www.sonatype.org/nexus/](http://www.sonatype.org/nexus/)

## Bibliotheken ##

Jadira
: [http://jadira.sourceforge.net/](http://jadira.sourceforge.net/)

JODA Date and Time API
: [http://www.joda.org/joda-time/](http://www.joda.org/joda-time/)

JSR-310 Development Site
: [http://www.threeten.org/](http://www.threeten.org/)

Spring Framework
: [http://projects.spring.io/spring-framework/](http://projects.spring.io/spring-framework/)


## Build Tools ##

Ant
: [http://ant.apache.org/](http://ant.apache.org/)

Gradle
: [http://www.gradle.org/](http://www.gradle.org/)

Maven
: [http://maven.apache.org/](http://maven.apache.org/)

## Codequalität ##

FindBugs Release 3.0.0 ist draußen und unterstützt nun Java 8.

Eclipse Plugins finden sich unter "Plugins".

Checkstyle
: [http://checkstyle.sourceforge.net/](http://checkstyle.sourceforge.net/)

FindBugs
: [http://findbugs.sourceforge.net/](http://findbugs.sourceforge.net/)

PMD
: [http://pmd.sourceforge.net/](http://pmd.sourceforge.net/)

SonarQube
: [http://www.sonarqube.org/](http://www.sonarqube.org/)

## Entwicklungsumgebungen ##

Eclipse Luna mit voller Unterstützung für Java 8 steht unter [http://www.eclipse.org](http://www.eclipse.org) bereit.

Eclipse Kepler
: [http://www.eclipse.org/kepler/](http://www.eclipse.org/kepler/)

IntelliJ IDEA
: [http://www.jetbrains.com/idea/](http://www.jetbrains.com/idea/)

Netbeans
: [https://netbeans.org/](https://netbeans.org/)

## Java 8 ##

Java 8 SDK
: [http://www.oracle.com/technetwork/java/javase/downloads/](http://www.oracle.com/technetwork/java/javase/downloads)

Java 8 Dokumentation
: [http://docs.oracle.com/javase/8/docs/](http://docs.oracle.com/javase/8/docs/)

## Kontinuierliches Bauen ##

Hudson
: [http://hudson-ci.org/](http://hudson-ci.org/)

Jenkins
: [http://jenkins-ci.org/](http://jenkins-ci.org/)

## Plugins ##

Eclipse Checkstyle Plugin
: [http://eclipse-cs.sourceforge.net/](http://eclipse-cs.sourceforge.net/)

Eclipse FindBugs Plugin
: [http://findbugs.cs.umd.edu/eclipse/](http://findbugs.cs.umd.edu/eclipse/)

Eclipse PMD Plugin
: [http://sourceforge.net/projects/pmd/files/pmd-eclipse/update-site/](http://sourceforge.net/projects/pmd/files/pmd-eclipse/update-site/)

Eclipse SonarQube Plugin
: [http://www.sonarsource.com/products/plugins/developer-tools/eclipse/](http://www.sonarsource.com/products/plugins/developer-tools/eclipse/)


Java 8 Support for Eclipse Kepler SR2
: [http://marketplace.eclipse.org/content/java-8-support-eclipse-kepler-sr2#.U7Eu9CgQv1U](http://marketplace.eclipse.org/content/java-8-support-eclipse-kepler-sr2#.U7Eu9CgQv1U)

Java 8 Facet for Web Tools for Eclipse Kepler SR2
: [http://marketplace.eclipse.org/content/java-8-facet-web-tools-eclipse-kepler-sr2#.U7EuyigQv1U](http://marketplace.eclipse.org/content/java-8-facet-web-tools-eclipse-kepler-sr2#.U7EuyigQv1U)

Java 8 support fpr m2e for Eclipse Kepler SR2
: [http://marketplace.eclipse.org/content/java%E2%84%A2-8-support-m2e-eclipse-kepler#.U7EvMygQv1U](http://marketplace.eclipse.org/content/java%E2%84%A2-8-support-m2e-eclipse-kepler#.U7EvMygQv1U)

Tomcat-Maven-Plugin
: [http://tomcat.apache.org/maven-plugin.html](http://tomcat.apache.org/maven-plugin.html)

## Server ##

Glassfish
: [http://glassfish.java.net](http://glassfish.java.net)

JBoss Wildfly
: [http://www.wildfly.org](http://www.wildfly.org)

Jetty
: [http://www.eclipse.org/jetty](http://www.eclipse.org/jetty)

Tomcat
: [http://tomcat.apache.org](http://tomcat.apache.org)
