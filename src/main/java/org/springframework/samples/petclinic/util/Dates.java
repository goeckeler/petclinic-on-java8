package org.springframework.samples.petclinic.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public final class Dates {
	/** hide utility constructor */
	private Dates() {
	};

	public static final Date fromLocal(final LocalDate date) {
		return Date.from(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	public static final LocalDate toLocal(final Date date) {
		Calendar day = Calendar.getInstance();
		day.setTime(date);
		
		int dayOfMonth = day.get(Calendar.DAY_OF_MONTH);
		int month = 1 + day.get(Calendar.MONTH);
		int year = day.get(Calendar.YEAR);
		
		return LocalDate.of(year, month, dayOfMonth);
	}
}
