package org.springframework.samples.petclinic.jsp;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.LoggerFactory;


public class DateTag extends SimpleTagSupport {
	
	private LocalDate value;
	private String pattern;
	
	public void setValue(final LocalDate date) {
		this.value = date;
	}
	
	public void setPattern(final String pattern) {
		this.pattern = pattern;
	}
	
	private String getPattern() {
		if (pattern == null) {
			pattern = "yyyy-MM-dd";
		}
		return pattern;
	}
	
	public void doTag() throws JspException, IOException {
		if (value == null) return;
		
		JspWriter out = getJspContext().getOut();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(getPattern());
		out.println(formatter.format(value));
	}
}
