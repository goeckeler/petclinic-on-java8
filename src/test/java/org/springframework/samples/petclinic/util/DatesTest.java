package org.springframework.samples.petclinic.util;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class DatesTest {

	@Test
	public void shouldConvertFromLocalToDate() {
		LocalDate time = LocalDate.of(2014, 12, 31);
		Date date = Dates.fromLocal(time);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		assertEquals(31, calendar.get(Calendar.DAY_OF_MONTH));
		assertEquals(Calendar.DECEMBER, calendar.get(Calendar.MONTH));
		assertEquals(2014, calendar.get(Calendar.YEAR));
		assertEquals(0, calendar.get(Calendar.HOUR_OF_DAY));
		assertEquals(0, calendar.get(Calendar.MINUTE));
	}
}
